#include "RandomPlayer.h"
#include "GameStateUtils.h"
#include <chrono>

namespace ttt {
	namespace ai {
		std::default_random_engine RandomPlayer::generator_ = std::default_random_engine(std::chrono::system_clock::now().time_since_epoch().count());

		Move RandomPlayer::getMove(const GameState& gameState) {
			const int moveCount = GameState::SIZE * GameState::SIZE - gameState.getTurnNumber();
			const std::uniform_int_distribution<int> distribution(1, moveCount);
			
			const int randomMove = distribution(generator_);
			int currentMove = 0;
			for (int x = 0; x < GameState::SIZE; ++x) {
				for (int y = 0; y < GameState::SIZE; ++y) {
					if (GameStateUtils::isEmpty(gameState, x, y)) {
						++currentMove;
						if (currentMove == randomMove) {
							return Move(x, y);
						}
					}
				}
			}

			throw std::exception("Invalid move count");
		}
	}
}
