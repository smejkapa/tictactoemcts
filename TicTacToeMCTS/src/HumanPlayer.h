#pragma once
#include "Player.h"

namespace ttt {
	class HumanPlayer
		: public Player {
	public:
		Move getMove(const GameState& state) override;
	};
}
