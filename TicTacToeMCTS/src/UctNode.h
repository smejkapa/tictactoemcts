#pragma once
#include "GameState.h"
#include <vector>
#include "GameStateUtils.h"

namespace ttt {
	namespace ai {
		struct UctNode {
		private:
			bool isFullyExpanded_;
			bool isTerminal_;
			int idx_;
		public:
			const GameState gameState;
			const Move move;
			UctNode* const parent;
			std::vector<UctNode> children;
			int value;
			int visitedCount;
			int winner;

			UctNode(const GameState& gameState, const Move& move, UctNode* const parent)
				: isFullyExpanded_(false)
				, isTerminal_(false)
				, idx_(0)
				, gameState(gameState)
				, move(move)
				, parent(parent)
				, value(0)
				, visitedCount(0) 
				, winner(-1) {
			}

			bool isFullyExpanded() const {
				return isFullyExpanded_;
			}

			bool isTerminal() const {
				return isFullyExpanded() && children.size() == 0 || isTerminal_;
			}

			UctNode& getLastChild() {
				return children[children.size() - 1];
			}

			bool tryExpand() {
				if (isFullyExpanded_ || isTerminal_) {
					return false;
				}

				while (true) {
					if (idx_ >= GameState::SIZE * GameState::SIZE) {
						isFullyExpanded_ = true;
						return false;
					}
					const int x = idx_ % GameState::SIZE;
					const int y = idx_ / GameState::SIZE;

					++idx_;

					if (GameStateUtils::isEmpty(gameState, x, y)) {
						const Move m = Move(x, y);
						GameState newState(gameState);
						newState.applyMove(m);
						bool newTerminal = false;
						int newWinner = -1;
						if (newState.isWinning(m)) {
							newTerminal = true;
							newWinner = newState.getActivePlayerIdx();
						} else if (GameStateUtils::isGameOverTurn(newState)) {
							newTerminal = true;
							newWinner = -1;
						}
						newState.nextTurn();
						UctNode newNode = UctNode(newState, m, this);
						newNode.isTerminal_ = newTerminal;
						newNode.winner = newWinner;
						
						children.push_back(std::move(newNode));

						return true;
					}
				}
			}
		};
	}
}
