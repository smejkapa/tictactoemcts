#pragma once
#include "Player.h"
#include "UctNode.h"

namespace ttt {
	namespace ai {
		class UctPlayer 
			: public Player {
		private:
			static const double C;
			long long timeLimit_;
			int iterCount_;

		public:
			explicit UctPlayer(long long timeLimit)
				: timeLimit_(timeLimit)
				, iterCount_(0) {
			}

			
			Move getMove(const GameState&) override;

		private:
			static UctNode& selectBestChild(UctNode& node);
			static UctNode& selectMostVisitedChild(UctNode& node);
			static UctNode& selectAndExpand(UctNode& node);
			static int playout(UctNode& state);
			static void backpropagateResults(UctNode& node, int value);

			static double ucb(const UctNode& node, int player) {
				const double exploit = static_cast<double>(node.value) / node.visitedCount;
				const double explore = C * sqrt(log(node.parent->visitedCount) / node.visitedCount);
				return player == 0 ? (exploit + explore) : (exploit - explore);
			}
		};
	}
}
