#include "GameStateUtils.h"
#include <iostream>

namespace ttt {
	void GameStateUtils::printField(const GameState& gameState) {
		for (int y = 0; y < GameState::SIZE; ++y) {
			for (int x = 0; x < GameState::SIZE; ++x) {
				switch (gameState.getField()[x][y]) {
				case 0:
					std::cout << ".";
					break;
				case 1:
					std::cout << "x";
					break;
				case 2:
					std::cout << "o";
					break;
				default:
					std::cout << "unknown field id";
				}
			}
			std::cout << std::endl;
		}
	}
}
