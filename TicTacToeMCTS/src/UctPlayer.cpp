#include "UctPlayer.h"
#include <chrono>
#include "Game.h"
#include "RandomPlayer.h"
#include <iostream>

namespace ttt {
	namespace ai {
		const double UctPlayer::C = 1.0 / std::sqrt(2);

		UctNode& UctPlayer::selectAndExpand(UctNode& node) {
			UctNode* currentNode = &node;
			while (true) {
				if (currentNode->tryExpand()) {
					return currentNode->getLastChild();
				} else if (currentNode->isTerminal()) {
					return *currentNode;
				}

				auto& bestChild = selectBestChild(*currentNode);

				currentNode = &bestChild;
			}
		}

		int UctPlayer::playout(UctNode& node) {
			int result;
			
			if (node.isTerminal()) {
				result = node.winner;
			} else {

				const GameState& state = node.gameState;
				Game::players_vec players;
				players.push_back(std::make_unique<RandomPlayer>());
				players.push_back(std::make_unique<RandomPlayer>());
				Game g(std::move(players), state);

				result = g.run();
			}

			return result == 0 ? 1 : result == 1 ? -1 : 0;
		}

		void UctPlayer::backpropagateResults(UctNode& node, int value) {
			UctNode* currentNode = &node;
			do {
				currentNode->visitedCount++;
				currentNode->value += value;
				currentNode = currentNode->parent;
			} while (currentNode != nullptr);
		}

		UctNode& UctPlayer::selectBestChild(UctNode& node) {
			const int player = node.gameState.getActivePlayerIdx();
			auto bestChild = node.children.end();
			double bestValue = 0;
			
			for(auto child = node.children.begin(); child != node.children.end(); ++child) {
				const double childUcb = ucb(*child, player);
				if (bestChild == node.children.end()
						|| (bestValue < childUcb && player == 0)
						|| (bestValue > childUcb && player == 1)) {
					bestChild = child;
					bestValue = childUcb;
				}
			}
			
			return *bestChild;
		}

		UctNode& UctPlayer::selectMostVisitedChild(UctNode& node) {
			auto bestChild = node.children.end();
			double mostVisits = 0;

			for (auto child = node.children.begin(); child != node.children.end(); ++child) {
				if (bestChild == node.children.end()
						|| mostVisits < child->visitedCount) {
					bestChild = child;
					mostVisits = child->visitedCount;
				}
			}

			return *bestChild;
		}


		Move UctPlayer::getMove(const GameState& state) {
			const auto startTime = std::chrono::high_resolution_clock::now();
			iterCount_ = 0;

			UctNode root = UctNode(state, Move(-1, -1), nullptr);

			while (true) {
				++iterCount_;

				UctNode& selectedNode = selectAndExpand(root);
				const int result = playout(selectedNode);

				backpropagateResults(selectedNode, result);

				const auto elapsed = std::chrono::high_resolution_clock::now() - startTime;
				if (std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() >= timeLimit_) {
					break;
				}
			}

			std::cout << iterCount_ << std::endl;

			UctNode& bestNode = selectMostVisitedChild(root);
			std::cout << bestNode.value << std::endl;
			std::cout << bestNode.visitedCount << std::endl;

			for (const auto & c : root.children) {
				std::cout << c.move.x << " " << c.move.y << " ";
			}
			std::cout << std::endl;
			for(const auto & c : root.children) {
				std::cout << c.visitedCount << " ";
			}
			std::cout << std::endl;
			for (const auto & c : root.children) {
				std::cout << c.value << " ";
			}
			std::cout << std::endl;
			for (const auto & c : root.children) {
				std::cout << ucb(c, 0) << " ";
			}
			std::cout << std::endl;

			std::cout << root.visitedCount << std::endl;

			return bestNode.move;
		}

	}
}
