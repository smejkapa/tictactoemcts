#pragma once
#include "Player.h"
#include "GameStateUtils.h"
#include <memory>
#include <vector>

#define PRINT_FIELD

namespace ttt {
	class Game {
	public:
		using players_vec = std::vector<std::unique_ptr<Player>>;

	private:
		GameState currentState_;
		players_vec players_;
		bool isPlayout_;

	public:
		explicit Game(players_vec&& players)
			: players_(std::move(players))
			, isPlayout_(true) {
		}

		Game(players_vec&& players, const GameState& gameState)
			: currentState_(gameState)
			, players_(std::move(players))
			, isPlayout_(true) {
		}

		void notPlayout() {
			isPlayout_ = false;
		}

		int run() {
			while (true) {
				if (GameStateUtils::isGameOverTurn(currentState_)) {
					return -1;
				}
				
				const Move move = players_[currentState_.getActivePlayerIdx()]->getMove(currentState_);
				currentState_.applyMove(move);
				
				if (currentState_.isWinning(move)) {
					return currentState_.getActivePlayerIdx();
				}

				currentState_.nextTurn();

#if defined(PRINT_FIELD)
				if (!isPlayout_)
					GameStateUtils::printField(currentState_);
#endif
			}
		}

		const GameState& getGameState() const {
			return currentState_;
		}
	};
}
