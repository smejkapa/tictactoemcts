#pragma once
#include "Player.h"
#include <vector>
#include <random>

namespace ttt {
	namespace ai {
		class RandomPlayer
			: public Player {
		public:
			Move getMove(const GameState&) override;
		private:
			static std::default_random_engine generator_;
		};
	}
}
