#include <iostream>
#include "Game.h"
#include "GameStateUtils.h"
#include "RandomPlayer.h"
#include "UctPlayer.h"

int main() {
	
	ttt::Game::players_vec players;
	players.push_back(std::make_unique<ttt::ai::UctPlayer>(1000));
	players.push_back(std::make_unique<ttt::ai::UctPlayer>(1000));
	//players.push_back(std::make_unique<ttt::HumanPlayer>());
	//players.push_back(std::make_unique<ttt::HumanPlayer>());
	//players.push_back(std::make_unique<ttt::ai::RandomPlayer>());
	
	ttt::Game g(std::move(players));
	g.notPlayout();
	const int winner = g.run();
	ttt::GameStateUtils::printField(g.getGameState());
	std::cout << "Player " << winner << " won" << std::endl;
	std::cout << "Done" << std::endl;
	return 0;
}
