#include "GameState.h"
#include <algorithm>

namespace ttt {
	int GameState::checkCount(int x, int y, char s, int count) const {
		if (field_[x][y] == s) {
			count++;
		} else {
			count = 0;
		}
		return count;
	}

	template<typename T>
	T clamp(T n, T a, T b) {
		return std::min(b, std::max(a, n));
	}

	int clampToSize(int n) {
		return clamp(n, 0, GameState::SIZE - 1);
	}

	bool GameState::isWinning(const Move& m) const {
		const char s = field_[m.x][m.y];

		// Left to right
		{
			const int y = m.y;
			int count = 0;
			const int minX = clampToSize(m.x - WIN_COUNT + 1);
			const int maxX = clampToSize(m.x + WIN_COUNT);
			for (int x = minX; x <= maxX; ++x) {
				count = checkCount(x, y, s, count);
				if (count >= WIN_COUNT) {
					return true;
				}
			}
		}

		// Top to bottom
		{
			const int x = m.x;
			int count = 0;
			const int minY = clampToSize(m.y - WIN_COUNT + 1);
			const int maxY = clampToSize(m.y + WIN_COUNT);
			for (int y = minY; y <= maxY; ++y) {
				count = checkCount(x, y, s, count);
				if (count >= WIN_COUNT) {
					return true;
				}
			}
		}

		// Right top to left bot
		{
			int count = 0;
			const int minX = clampToSize(m.x - WIN_COUNT);
			const int maxX = clampToSize(m.x + WIN_COUNT - 1);
			const int minY = clampToSize(m.y - WIN_COUNT + 1);
			const int maxY = clampToSize(m.y + WIN_COUNT);
			for (int x = maxX, y = minY; x >= minX && y <= maxY; --x, ++y) {
				count = checkCount(x, y, s, count);
				if (count >= WIN_COUNT) {
					return true;
				}
			}
		}

		// Left top to right bot
		{
			int count = 0;
			const int minX = clampToSize(m.x - WIN_COUNT + 1);
			const int maxX = clampToSize(m.x + WIN_COUNT);
			const int minY = clampToSize(m.y - WIN_COUNT + 1);
			const int maxY = clampToSize(m.y + WIN_COUNT);
			for (int x = minX, y = minY; x <= maxX && y <= maxY; ++x, ++y) {
				count = checkCount(x, y, s, count);
				if (count >= WIN_COUNT) {
					return true;
				}
			}
		}

		return false;
	}
}
