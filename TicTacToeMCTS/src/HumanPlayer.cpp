#include "HumanPlayer.h"
#include <iostream>
#include "GameStateUtils.h"

namespace ttt {
	Move HumanPlayer::getMove(const GameState& state) {
		GameStateUtils::printField(state);
		std::cout << "Enter yor move x y: ";
		int x, y;
		std::cin >> x >> y;
		return Move(x, y);
	}
}
