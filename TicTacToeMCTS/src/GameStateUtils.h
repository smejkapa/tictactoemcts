#pragma once

#include "GameState.h"

namespace ttt {
	class GameStateUtils {
	public:
		static void printField(const GameState& gameState);
		static bool isEmpty(const GameState& gameState, int x, int y) {
			return gameState.getField()[x][y] == 0;
		}

		static bool isGameOverTurn(const GameState& gameState) {
			return gameState.getTurnNumber() >= GameState::SIZE * GameState::SIZE;
		}
	};
}
