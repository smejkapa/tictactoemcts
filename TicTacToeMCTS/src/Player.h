#pragma once
#include "GameState.h"
#include "Move.h"

namespace ttt {
	class Player {
	public:
		virtual ~Player() = default;
		virtual Move getMove(const GameState&) = 0;
	};
}
