#pragma once
#include <algorithm>
#include <array>
#include "Move.h"
#include <cassert>

namespace ttt {
	class GameState {
	public:
		static constexpr int SIZE = 9;
		static constexpr int WIN_COUNT = 5;
		using field_t = std::array<std::array<char, SIZE>, SIZE>;

	private:
		field_t field_;
		char activePlayerIdx_;
		int turnNumber_;

		int checkCount(int x, int y, char s, int count) const;

	public:
		GameState()
				: activePlayerIdx_(0)
				, turnNumber_(0) {
			for (int x = 0; x < SIZE; ++x) {
				for (int y = 0; y < SIZE; ++y) {
					field_[x][y] = 0;
				}
			}
		}

		void applyMove(const Move& m) {
			assert(field_[m.x][m.y] == 0 && "Invalid move detected");
			field_[m.x][m.y] = activePlayerIdx_ + 1;
		}

		bool isWinning(const Move& m) const;

		const field_t& getField() const {
			return field_;
		}

		char getActivePlayerIdx() const {
			return activePlayerIdx_;
		}

		int getTurnNumber() const {
			return turnNumber_;
		}

		void nextTurn() {
			activePlayerIdx_ = 1 - activePlayerIdx_;
			++turnNumber_;
		}
	};
}
